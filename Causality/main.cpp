#include <time.h>
#include <iostream>
#include <string>


#include <iomanip>
#include<fstream>
#include <map>
#include <cmath>
#include <algorithm>

using namespace std;
#include "Net.h"
#include "Net.cpp"
#include "bdd.h"
#include "fdd.h"

#include "RdPBDD.h"

double getTime(){
	return (double)clock() / (double)CLOCKS_PER_SEC;
}
  
int main(int argc, char** argv) {

if (argc <=2 or std::string(argv[1]).find("help")!=-1)
cout <<"/**************************************************************/\n/********************         help         ********************/ \n -To generate the reachability graph: use the option -r <filename.net>. \n -To generate test paths covering the given observable transitions t1,t2...tn: use the option -o <filename.net> <fileobs.txt >. \n -To generate test paths using structural analysis: use the option -a <filename.net>\n -To generate the conplete SOG corresponding to obs trans: use the option -c <filename.net>/**************************************************************/"<<endl;


else{
  	string option= std::string (argv[1]);
  	cout << "nom fichier source " << endl;
	cout<<"Fichier net : "<<argv[2]<<endl;
	net R(argv[2]);	
	cout << "parser...done" << endl;
	set <vector<int>> abstrait;
    int pp=string(argv[2]).find(".n");
    int ps=string(argv[2]).rfind("/");
    string nom=string(argv[2]).substr(ps+1,pp-ps-1);
    double d,tps;  
    int b = 32;
    map<int,int> obs;
    set<int> unobs;
    set<vector<int>> chemins;
   
    d = getTime();




// option -a for structural analysis 
if(!option.compare("-a")or !option.compare("-o")or !option.compare("-c")){
 
 if (!option.compare("-a")or !option.compare("-c")){
    unobs=R.calcul1();
    for (long unsigned int i = 0; i < R.transitions.size(); i++ )
    {
         if ((unobs.find(i))== unobs.end())
         {
             obs.insert({i,1}); 
         }  
    }
  } 
    
 if (!option.compare("-o")){      //lire les trnansitions observable a partie d'un fichier 
 ifstream flux(argv[3]); 
     string ligne;
	if (flux){
	while(getline(flux, ligne))
		{
		int tr =stoi(ligne.substr(1)); // le ligne sous forme de t1 prendre le1
		 obs.insert({tr,1}); 
		}
 
	}
	for (long unsigned int i = 0; i < R.transitions.size(); i++ )
    {
         if ((obs.find(i))== obs.end())
         {
             unobs.insert(i); 
         }  
    }
 }


  
    RdPBDD DR(R,obs,unobs, b);
    cout<<"RdpBDD construit"<<endl;
    MDGraph g;


if(!option.compare("-c") ) { 
	DR.complete_sog(g,obs);
      	g.generate_Dotfile_of_reachability_graph("complete_sog_"+nom);
      	 cout << " Result in ./result/complete_sog_"+nom+".dot" << endl;
}

 ofstream monFlux("./result/result_no_opt_"+nom+".txt");

 int n=0;
if(!option.compare("-a")or !option.compare("-o"))
{ chemins= DR.chem_obs(g,obs);
 monFlux<<"le nombre de chemins observés: "<<chemins.size()<<endl;
 for (auto i: chemins)
 {
  n++;
  monFlux<<"le "<<n<<" ch: " <<i.size()<< " tr."<<endl; //result in file result/result_no_opt.txt"

  vector<int> chem_abs;
  chem_abs= DR.chem_abs(i,g);
  
  abstrait.insert(chem_abs);
}

  
  
 tps = getTime() - d;

  
 cout << " Temps de construction du graphe d'observation " << tps << endl;
 g.printCompleteInformation();
 cout<< "transition "<<R.transitions.size()<<endl;
 cout<< "places "<<R.places.size()<<endl;
 cout<< "trans obs "<<obs.size()<<endl;
 cout<< "--- abstract path ----"<<endl;
 set<int> visit;
 float somme =0 ;
 float ecart=0; 


 for (auto i: abstrait)
  {
 
  for (auto tr:i)
   {
    visit.insert(tr);
   }

     somme=somme+i.size();
     
 }
  
 
    
float moy= somme/(abstrait.size());
cout<< "nb de chemins: "<<abstrait.size()<<endl;
cout<< "nb moyen de transitions par chemin: "<<moy<<endl;

    
for (auto i: abstrait)
 {

ecart = ecart+ (((i.size()-moy)*(i.size()-moy))); 

     
  }

    ecart =pow((ecart/abstrait.size()),0.5);
    cout<< "ecart type: "<<ecart<<endl;

      cout<< "nb of covered transitions: "<< visit.size()<<endl;  
       cout << " Result in  " << "./result/result_no_opt_"+nom+".txt" << endl;
	return 0;

} 



 }

else if (!option.compare("-r")){
    for (long unsigned int i = 0; i < R.transitions.size(); i++ )
    {

             obs.insert({i,1});  
    }	
  
    RdPBDD DR(R,obs,unobs, b);
    MDGraph g;
    DR.complete_sog(g,obs);
    g.generate_Dotfile_of_reachability_graph("reach_"+nom);
   return 0;
    }

 
 }
 }
